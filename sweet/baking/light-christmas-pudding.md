# Nigel Slater's Light Christmas Pudding

* 280g sultanas
* 280g raisins or currants
* 120g dried figs
* 100g candied peel
* 80g dried apricots
* 60g glace cherries
* 120ml brandy
* 2 apples or quinces
* 2 oranges, juice and zest
* 5 eggs
* 200g suet
* 280g dark muscovado sugar
* 200g fresh breadcrumbs
* 140g self-raising flour
* 1tsp mixed spice

Makes two.

Steam for 3 hours, store then steam for 2.5-3 to reheat.
