# Financiers

* 100g butter
* 1 pinch of salt
* 4 egg whites
* 60g flour, sifted
* 90g ground almonds
* 150g icing sugar

Makes a dozen financiers

1. Preheat oven to 200C
1. Melt the butter and allow to cool
1. In a mixing bowl, mix the flour, almonds, sugar and salt. Add the egg whites one by one, mixing each time with a wooden spoon.
1. Keep mixing while adding the melted butter
1. Lower the oven temperature to 180C. Pour the batter into buttered moulds and cook for about 15 minutes, keeping an eye on them to make sure they don't overcook. 
1. Once cooked, remove the financiers from their moulds and allow to cool on a rack.

