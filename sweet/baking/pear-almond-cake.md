# Pear and almond cake with almond crunch topping

* 175g softened butter
* 150g golden caster sugar
* 1tsp vanilla
* 2 eggs
* 220g self-raising flour
* 1/2tsp bicarbonate of soda
* 1tsp ground nutmeg
* 140ml soured cream
* 1 lemon, zest
* 50g ground almonds
* 2 firm pears
* A little lemon juice

For the topping:

* 50g butter
* 50g light muscovado sugar
* 2tbsp double cream
* 75g flaked almonds

1. Line a 20cm springform tin. Preheat oven to 180 deg C (160 fan)
1. Cream butter, sugar and vanilla. Mix eggs and add flour, salt, bicarb
and nutmeg.
1. Fold in soured cream, zest and almonds.
1. Peel and core pears. Toss with juice.
1. Spread half the cake mixture into the tin. Add the pears and the remaining
mixture.
1. Bake for 40 minutes
1. Melt butter and stir in sugar, cream and almonds.
1. Remove cake from oven and pour the almond mixture over the top. 
1. Bake for 20-25 minutes further.
