# Welsh cakes

* 225g of self-raising flour, or 225g plain flour and 1 tsp baking powder
* 1 pinch of salt, ideally Welsh sea salt
* 100g of butter, or margarine, plus extra for cooking (again, ideally Welsh)
* 50g of caster sugar, plus extra for sprinkling
* 50g of currants
* 1 egg, beaten with 3 tbsp milk

## Method

1. Mix the flour and salt together in a large bowl and rub in the margarine or butter. Add the sugar and currants and stir well
1. Pour the egg mixture in and mix until you have a stiff dough
1. Roll the dough out on a lightly floured board until 5mm (1/4 inch) thickness and stamp out rounds with a pastry/biscuit cutter
1. Heat the griddle over a medium heat until hot and grease with a little butter – the baking griddle should be well-greased, and then heated until a little water sprinkled on the surface skips about in balls, evaporating. A heavy cast iron frying pan makes a good substitute. Cook the cakes for about 3 to 4 minutes each side, until they are golden brown and have risen slightly
1. Serve immediately sprinkled with a little extra caster sugar

## Tips

1. Roll them quite thinly - a pound coin is enough
1. Don't overcook them - bouncy is OK (but not squishy)
1. Use the 68mm cutter. Makes approximately 20.
