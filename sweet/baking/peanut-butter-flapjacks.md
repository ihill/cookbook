# Peanut Butter Flapjacks

* 175g crunchy peanut butter
* 7 tbsp golden syrup
* 75g raisins
* 75g light soft brown sugar
* 2tbsp sunflower oil
* 175g rolled porridge oats

1. Preheat oven to 180 deg C (160 fan). Grease and line a 20cm square tin.
1. Gently heat peanut butter, sugar, golden syrup and oil until melted. Stir in raisins.
1. Turn out into tin and level with the back of a spoon
1. Bake for 25 minutes
1. Cut once slightly cooled but still warm.
1. Turn out once cold.

## Decoration

* 100g milk chocolate
* 40g peanut butter
* 25g lightly roasted peanuts, roughly chopped
* 2tbsp milk

1. Melt chocolate with other ingredients
1. Drizzle over cooled flapjack
