# Chocolate Loaf Cake

* 225g butter
* 225g caster sugar
* 4 eggs
* 175g self-raising flour
* 50g cocoa powder
* 1tsp baking powder
* 2-3tbsp milk
* 100g chopped chocolate

25-30 minutes at 160degC fan.
