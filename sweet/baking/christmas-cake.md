# Christmas Cake

* 340g plain flour
* 220g butter
* 220g soft brown sugar
* 50g ground almonds
* 110g glace cherries
* 30g walnuts
* 110g mixed peel
* 340g sultanas
* 340g currants
* 110g raisins
* 1 lemon, zest and juice
* 1 orange, zest and juice
* 4 large eggs
* 50g golden syrup
* 1tsp rosewater
* 1tsp orange flower water
* 1tsp noyoau essence
* 1tsp ground nutmeg
* 1tsp ground ginger
* 1tsp ground cinnamon
* 1tsp ground cloves

* 50ml sherry
* 50ml brandy
* 50ml port

1. Put fruit in a saucepan and cover with cold water. Bring to boil then strain 
   immediately. Dry overnight.
1. Whisk eggs, syrup, zest, juice, syrup, orange flour water and noyau.
1. Cream butter and sugar.
1. Mix in flour, egg mixture and fruit.
1. Grease and line an 8" tin.
1. Bake in centre shelf at 160deg for one hour.
1. Reduce to 150deg for a further three hours.
1. Cool in the tin. prick, pour over spirits.
1. Wrap in greaseproof and foil for storage.
1. Mature!
