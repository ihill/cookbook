# American Chocolate Chip Cookies

* 1/4cup brown sugar
* 1/2cup white sugar
* 1/2tsp vanilla
* 1 egg
* 1/2cup butter
* 1/4tsp baking soda
* pinch salt
* 1cup + 2tbsp flour
* chocolate chips, to taste


Bake 2in apart for 12 minutes at 375 deg F or until golden.

