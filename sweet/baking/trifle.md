# Classic Trifle

## Sponge

* 125g plain flour
* 125g golden caster sugar
* 3 eggs
* vanilla

1. Line a tin with oil and baking parchment and dust with flour.
1. Mix the eggs, sugar and vanilla, beat with an electric whisk for 10 minutes 
until thick.
1. Gently fold in the flour in two stages.
1. Fold in a tbsp of lukewarm water
1. Bake at 180 degC fan until golden.

