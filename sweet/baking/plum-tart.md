# Plum and frangipane tart

![Plum tart](plum-tart.jpg)

* sweet pastry
  * 250g flour
  * 125g butter
  * 50g sugar (this is half the usual because the filling is so sweet)
  * 1 egg
  * splash very cold milk
* frangipane
  * 175g soft butter
  * 175g caster sugar
  * 3 eggs
  * 175g ground almonds
  * 40g flour
  * almond extract or lemon zest
* 2-300g quetschen

1. Make the pastry as usual and blind bake at 180. Allow to cool slightly.
1. Cream the butter and sugar, add the eggs then the remaining frangipane
   ingredients. Spread over the pastry base.
1. Cut the plums into quarters and arrange tightly on top of the frangipane,
   pressing lightly.
1. Bake for 45-50 minutes at 160 (cover with foil later if needed)

The fruit really shrinks so make sure you really pack it on.

