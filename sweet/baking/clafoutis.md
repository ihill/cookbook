# Cherry Clafoutis

* 500g cherries
* 80g sugar
* 100g flour
* pinch of salt
* 3 eggs
* 250ml milk

1. Combine flour, 30g of the sugar, and salt, mix in the eggs and milk. 
Leave to sit for a few hours.
1. Meanwhile, wash the cherries and give them a little squash to break the 
skins. Macerate them in the rest of the sugar and a splash of amaretto.
1. Butter a dish and line with a little sugar or ground almonds.
1. Pour over the cherries and batter.
1. Bake for 40mins at 180 fan.
1. Sprinkle a little demerara when baked.
