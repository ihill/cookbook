# Clementine drizzle cake

* 225g self-raising flour
* 225g caster sugar
* 225g soft butter
* 4 eggs
* 2tbsp ground almonds
* 4-5 clementines
* 0.25tsp baking powder

1. Cream the butter and sugar, then add eggs one by one. 
1. Add the flour, extra baking powder, almonds and the zest of two clementines.
1. Mix to a smooth batter, adding a little milk if needed.
1. Pour into a lined loaf tin.
1. Bake at 180 for 40-45 minutes
1. Juice the clementines and sieve. Reduce to a thin syrup with 2tbsp sugar.
1. Add the zest and juice of one more clementine to refresh the orange flavour.
   You want to end up with a good 5-6 tbsp of syrup.
1. Let the cake rest for a few moments then prick all over and add the syrup.

Tips:
* I blitzed some candied orange peel with the sugar for more orange flavour.
* I tried a 175g/3 egg recipe but this didn't quite fill the tin.
* Don't over-reduce the syrup or it will caramelise and you'll lose the fruit.

