# Peanut butter chocolate chip cookies

Makes 20-24 cookies. Normally do half batch (12 cookies)

Ingredients:
* 1 cup (226 g) unsalted butter softened
* 1 cup (260g) creamy peanut butter
* 1 cup (200 g) light brown sugar firmly packed
* ½ cup (100 g) granulated sugar
* 1 egg + 1 large egg yolk (for half batch, just 1 egg)
* 1 teaspoon vanilla extract
* 3 cups (375 g) all-purpose flour
* 1 Tablespoon cornstarch
* ½ teaspoon baking soda
* ¾ teaspoon salt
* 200g chocolate chips

Method:
1. Combine butter, peanut butter, and sugars in a large mixing bowl and use an electric mixer to beat until light and fluffy.

1. Add egg, egg yolk, and vanilla extract and stir until combined.

1. In a separate, medium-sized bowl, whisk together flour, cornstarch, baking soda, and salt.

1. With mixer on low-speed, gradually stir dry ingredients into wet until completely combined (be sure to scrape the sides and bottom of the bowl and no dry spots should be remaining).

1. Add chocolate and use a spatula to stir into the cookie dough until well-incorporated.

1. Cover bowl with plastic wrap and refrigerate for at least 30 minutes. While dough chills, preheat oven to 180C and line baking sheets with parchment paper.

1. Once dough is chilled, remove from refrigerator and scoop into large ⅓-cup sized balls and roll between your palms to make a smooth ball. Place on parchment-lined baking sheets, spacing cookies at least 2” apart.

1. Transfer to 180C oven and bake for 13-15 minutes (edges may be just beginning to turn light golden brown). Allow cookies to cool completely on baking sheet before removing and enjoying.


Notes:
1. I usually reduce the sugar a bit compared to the full recipe - approx 25% down.

1. Same for chocolate - 100-150g is Plenty 

1. Could add chopped peanuts, especially if using smooth peanut butter.


Source: https://sugarspunrun.com/peanut-butter-chocolate-chunk-cookies/#recipe 
