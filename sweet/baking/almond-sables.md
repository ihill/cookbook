# Almond sables

A perfect petit four!

* 100g softened butter
* 50g caster sugar
* 100g flour
* 50g ground almonds

1. Cream butter and sugar, add some orange zest if you like.
1. Gently mix in flour, almonds and a pinch of salt. Form a soft dough and 
   chill.
1. Roll out and cut into small shapes. 
1. Bake for just under 15 minutes at 170deg (without fan).
