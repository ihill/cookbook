# Carrot Cake Traybake

* 150g dark brown sugar
* 150g golden caster sugar
* 300ml sunflower oil
* 3 eggs
* 300g plain flour
* 1tsp bicarbonate of soda
* 1tsp baking powder
* 1tsp ground cinnamon
* 1/2tsp ground ginger
* 1/2tsp ground nutmeg
* 1/2tsp salt
* 300g carrots, grated
* 100g walnuts, chopped

For the topping:
* 100g full fat cream cheese
* 30g unsalted butter, softened
* 200g icing sugar

1. Preheat oven to 170deg C. Line a 23x33cm tray.
1. Whisk sugars, oil and eggs until smooth. Slowly add flour, bicarb,
baking powder, spices and salt. Beat until well mixed.
1. Stir in grated carrots and chopped walnuts.
1. Pour into lined tin and bake for 35-40 minutes
1. Allow to cool for 20 minutes in the tin and then turn out onto a rack.
1. Mix topping ingredients and ice.

