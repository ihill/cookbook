# Date and walnut pudding

* 175g self-raising flour
* 75g suet
* 2tbsp sugar
* 50g golden syrup or treacle
* 100g dates
* 100g walnuts
* zest of one lemon
* 1 egg
* 150ml milk

1. Finely chop the walnuts and dates
1. Combine the dry ingredients in a bowl with a pinch of salt
1. Beat the egg into the milk and add the zest of one lemon
1. Mix all together
1. Steam for three hours in a 1.5pint basin

