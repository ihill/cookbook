# Slow Cooker Rice Pudding

* 0.5 cup rice
* 3 cup milk

1. Cook in the instant pot on the porridge function.
1. When cooked, mix in one egg, gradually.
1. Heat until thickened.
1. Leave to sit for a while to get a nice thick texture. 
