# Crumble topping

* 100g flour
* 100g butter (cold)
* 50g brown sugar
* 50g rolled oats
* Pinch of salt

1. Mix flour and sugar
1. Rub the butter into the flour/sugar mix
1. Add the oats to the mix
1. Spread over filling and back for about 40 minutes at 180C

Notes:
1. Makes enough for about 4 portions. For the purple LeCreuset make 1.5x quantity, for the big crumble dish 1.5-2x
1. Refigerate crumble mix for at least an hour for best results
