# Pumpkin Pie

* 2 eggs + 1 yolk
* 1/2 cup packed brown sugar (~85g)
* 1/3 cup white sugar (~67g)
* 1/2 tsp salt
* 2 tsp cinnamon
* 1 tsp ground ginger
* 1/4 tsp ground cloves
* 1/4 tsp ground nutmeg
* 1/8 tsp ground cardamom
* 1/2 tsp lemon zest
* 1 tin pumpkin puree (or equivalent quantity fresh)
* 1+1/2 cup cream (360g)
* Pre-prepped or fresh shortcrust pastry (enough for a 22-23cm pie dish)

1. Preheat the over to 200C fan
1. Beat the eggs together
1. Add sugar, salt and spices, and mix well
1. Mix in the pumpkin
1. Stir in the cream
1. Line a buttered 22-23cm pie dish or tart tin with the pastry (no need to blind bake)
1. Pour the mix into the pastry case
1. Bake for 15 minutes @200C fan
1. Turn the heat down to 160C and bake for a further 45-55 minutes, until the filling is set but still slightly wobbly

Notes:
1. Original recipe from https://www.simplyrecipes.com/recipes/suzannes_old_fashioned_pumpkin_pie/
