# Lemon Honey Cheesecake

* 500g full-fat cream cheese
* 170g Greek yoghurt (strained)
* 150ml double cream
* 2-3 tbsp good honey
* 2 lemons, zest and juice

1. Whip the cream until barely soft peaks.
1. Mix in the cream cheese and yoghurt. Add a pinch of salt.
1. Add the honey, juice and zest.
1. Spread over a biscuit base and chill for at least 8 hours.

