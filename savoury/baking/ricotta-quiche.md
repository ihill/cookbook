# Ricotta and Salmon Quiche

For the pastry:
 * 250g flour
 * 125g butter
 * 1 egg
 * pinch salt
 * 40ml cold water

For the custard:
 * 250g ricotta
 * 2 eggs
 * ~100ml cream
 * Salt, pepper and nutmeg
 * Parmesan

Blind bake as usual. Add spinach and a nice piece of flaked salmon. Peas are 
nice too.

Bake for 20-30minutes untill wobbly.
