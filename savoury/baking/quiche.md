# Christiane's famous quiche!

Fier 1 quiche brauchs du:
- 1 pate brisée (Herta)
- 6 ganz Eeer
- 25cl Rahm
- 25 cl Mëllech
- frésche Péiterséilech
- Salz a Peffer
- 200 gr Speck
- 200 gr Emmenthaler

Lee den ausgerullten Deeg mat dem Papeier (an dem en ageweckelt ass) an eng 
grouss Tarteform (net agefett). Schneit de Papeier ronderemm ewach. Mat enger 
Forschett Lächer an den Deeg pikken.

Dann an enger Schossel di 6 Eeer, d'Rahm an d'Mëllech mateneen verméschen. 
Salzen, päfferen. Dee kleng gerappten Péiterséilech (eng hallef Bott) drann 
verréieren.

Op den Deeg di 200 gr. Speck (dés de lardons) verdeelen. Duerno de gerappten 
Emmenthaler driwer grimmelen.

Zulescht deng Flössegkeet nach eng Kéier opklappen an driwer schédden.

Dann geet daat ganzt fier +40 Minutten bei +/- 190 Grad Oemloft an de 
Schäffchen. Du kanns der och 2 matteneen machen.

