# Pizza

## Pre-ferment

* 50g water
* 50g flour

1. Mix and leave overnight

## Pizza base

* Pre-ferment
* 400g flour
* 272g water (72% hydration)
* <5g salt

Makes two medium pizzas.
