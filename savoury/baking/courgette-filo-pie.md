# Courgette and chickpea filo pie

* 1/2 tsp cumin
* 1 small onion
* 1/2 tsp tumeric
* generous 1/2 tsp nigella seeds
* 1 tsp ground coriander
* 3 courgettes
* 125g basmati rice
* 500ml stock
* 2x 400g tins chickpeas
* 100g melted butter
* 200g filo

1. Fry onion and cumin, add remaining spices and cubed courgette. Fry until 
   soft.
1. Add the rice, stir and then add the stock little by little.
1. Once the rice is cooked add the chickpeas and set aside.
1. Brush the larger springform with butter and layer with 2/3rds of the filo.
1. Add the filling.
1. Crinkle more filo on top. Scatter a little more nigella.
1. Bake for 20mins at 180deg

Notes

* I needed less butter but more oven time.
