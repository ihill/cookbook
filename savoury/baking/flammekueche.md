# Flammekueche

Makes four small-to-medium.

* 250g flour
* 10ml olive oil
* pinch salt
* 150ml water
* streaky bacon
* onion
* creme fraiche
* Munster or Reblechon

1. Mix the flour, oil, salt and water
1. Knead well and rest for an hour
1. Fry bacon lardons and add thinly sliced onion for the last
few minutes
1. Roll the dough to a very thin rectangle
1. Spread with seasoned creme fraiche
1. Add the lardons, onions and cheese
1. Bake in a hot oven for 10-15 minutes until golden

