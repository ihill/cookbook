# Dressing for sweet potato and chickpea salad

* 1/4 cup tahini
* 1 Tbsp maple syrup
* 1 small lemon, juiced (~2 Tbsp juice as original recipe is written)
* 1-2 Tbsp hot water (to thin)

1. Mix all the ingredients together and adjust texture with water as required

Source

https://minimalistbaker.com/roasted-sweet-potato-chickpea-salad/
