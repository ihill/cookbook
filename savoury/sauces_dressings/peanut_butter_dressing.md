# Peanut butter dressing

* 2-3 tbsp peanut butter (more for a thicker dressing)
* 3 tbsp rice vinegar
* 2 tbsp soy sauce
* 2 tbsp lime juice (approx 1 lime)
* 1 tsp sesame oil
* 1 large tsp honey

optional:
* 1 tbsp fresh ginger minced
* 1/2 clove garlic minced
* small tsp of sriracha/chili sauce


1. Mix all the ingredients together. Use a blender if you can't be bothered to mince the ginger and garlic. Use a bit of hot water if the peanut butter is hard to dissolve. 
1. Adjust the thickness with a bit of water or lime juice if the dressing is too thick

Sources:

https://www.gimmesomeoven.com/thai-peanut-dressing/

https://www.culinaryhill.com/thai-peanut-dressing/
